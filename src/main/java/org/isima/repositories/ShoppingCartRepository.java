package org.isima.repositories;

import org.isima.models.Rule;
import org.isima.models.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ShoppingCartRepository extends JpaRepository<Rule, Long> {

    @Query("select s from ShoppingCart s where s.id = ?1 AND s.email = ?2")
    List<ShoppingCart> findAllProduct(int id, String email);


    @Transactional
    @Modifying
    @Query(value = "INSERT into shoppingcart (key, id, email, barcode) values (?4 ,?1 , ?2 , ?3)", nativeQuery = true)
    void addProduct(int id,String email, String barCode, int key);

    @Transactional
    @Modifying
    @Query("DELETE FROM ShoppingCart WHERE id = ?1 AND email = ?2 AND barcode = ?3")
    void deleteProduct(int id,String email, String barCode);
}
