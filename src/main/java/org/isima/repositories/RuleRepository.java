package org.isima.repositories;

import org.isima.models.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RuleRepository extends JpaRepository<Rule, Long> {

    @Query("select r from Rule r where r.name = ?1 and r.min_bound < ?2 order by r.min_bound desc ")
    List<Rule> findPoint(String name, double min_bound);
}
