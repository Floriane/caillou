package org.isima.services;

import org.isima.components.OpenFoodFactsComponent;
import org.isima.models.Nutriment;
import org.isima.models.Product;
import org.isima.models.QualityResult;
import org.isima.models.Rule;
import org.isima.repositories.RuleRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class QualityService {

    @Autowired
    private RuleRepository ruleRepository;

    // evaluate the quality of the product with the barcode entered.
    public QualityResult getQuality(String barCode) {

        QualityResult retour;

        try
        {
            // try to get the product on format Json with the Api openfoodfact
            JSONObject productJson = OpenFoodFactsComponent.getProductApi(barCode);
            JSONObject nutriments = productJson.getJSONObject("product").getJSONObject("nutriments");
            // try to parse json product to the model product
            Product product = new Product(
                    Double.parseDouble(nutriments.getString("energy_100g")),
                    nutriments.getDouble("saturated-fat_100g"),
                    nutriments.getDouble("sugars_100g"),
                    nutriments.getDouble("salt_100g"),
                    nutriments.getDouble("fiber_100g"),
                    nutriments.getDouble("proteins_100g")
            );

            // for each nutriment of the product, calculate mark end complete if the product is positive or negative
            for (int i = 0; i < product.nutriments.size(); i++) {
                Rule rule = ruleRepository.findPoint(product.nutriments.get(i).name, product.nutriments.get(i).value).get(0);
                product.nutriments.get(i).mark = rule.getPoints();
                product.nutriments.get(i).isPositif = (rule.getComponent().equals("P"));
            }

            // calculate total mark and find advantages and default of the product
            retour = getQualityResultOfProduct(product);
        }
        catch (Exception e) // an error with API request
        {
            retour = null;
        }
        return retour;
    }

    // calculate total mark and find advantages and default of a product with nutriment with mark complete
    public QualityResult getQualityResultOfProduct(Product product)
    {
        QualityResult qualityResult = new QualityResult();
        // for each nutriment of the product update mark and advantages and default
        for(int i = 0; i < product.nutriments.size(); i++)
        {
            Nutriment nutriment = product.nutriments.get(i);
            if(nutriment.isPositif)     // substract mark if nutriment is positive
            {
                qualityResult.mark = qualityResult.mark - nutriment.mark;
            }
            else                        // add mark else
            {
                qualityResult.mark = qualityResult.mark + nutriment.mark;
            }
            int defAdv = isQuality(nutriment);
            if(defAdv == -1) // if nutriment is default updates defaults list
            {
                qualityResult.defaults.add(nutriment.name);
            }
            else{
                if(defAdv == 1) // if nutriment is advantage updates advantages list
                {
                    qualityResult.advantages.add(nutriment.name);
                }
            }
        }
        return qualityResult;
    }

    // return if the nutriment is default, advantage or nothing: -1 = Default ; 1 = Advantage ; 0 = nothing;
    public int isQuality(Nutriment nutriment)
    {
        int defAdv = 0;
        if(nutriment.isPositif)
        {
            if(nutriment.mark >= 2)
            {
                defAdv = 1;
            }
            else
            {
                if(nutriment.mark <= 0)
                {
                    defAdv = -1;
                }
            }
        }
        else
        {
            if(nutriment.mark <= 3)
            {
                defAdv = 1;
            }
            else
            {
                if(nutriment.mark >= 7)
                {
                    defAdv = -1;
                }
            }
        }
        return defAdv;
    }
}
