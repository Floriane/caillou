package org.isima.services;


import org.isima.models.QualityResult;
import org.isima.models.ShoppingCart;
import org.isima.repositories.ShoppingCartRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private QualityService qualityService;

    // counter for ShoppingCar unique key
    private static int keyCompteur = 3;

    // add a product with barCode entered in the ShoppingCare with the right id and email entered
    public void addProduct(int id,String email,String barCode)
    {
       shoppingCartRepository.addProduct(id,email,barCode, keyCompteur);
       keyCompteur ++;
    }

    // delete product with barCode entered in the ShoppingCare with the right id and email entered
    public void deleteProduct(int id,String email,String barCode)
    {
        shoppingCartRepository.deleteProduct(id,email,barCode);
    }

    // evaluate the quality of the ShoppingCare with the id and email entered.
    public QualityResult getQuality(int id, String email) {

        QualityResult result = new QualityResult();

        try {
            // find all product of the right ShoppingCare
            List<ShoppingCart> products = shoppingCartRepository.findAllProduct(id, email);
            QualityResult resultCour;

            if(products.size() > 0) // if the ShoppingCare exist
            {
                // for each product in the ShoppingCare, get qualityResult of the product and update the qualityResult total
                for (int i = 0; i < products.size(); i++) {
                    resultCour = qualityService.getQuality(products.get(i).getBarCode());
                    result.mark = result.mark + resultCour.mark;
                    result.advantages.addAll(resultCour.advantages);
                    result.defaults.addAll(resultCour.defaults);
                }
            }
            else // if ShoppingCare doesn't exist in the database
            {
                result = null;
            }
        }
        catch (Exception e){ // an error with API request of a product
            result = null;
        }

        return result;
    }
}
