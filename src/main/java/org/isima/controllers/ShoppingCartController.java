package org.isima.controllers;

import io.swagger.annotations.*;
import org.isima.models.QualityResult;
import org.isima.services.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/shoppingcart")
@Api(value = "ShoppingCart management", description = "Operations to manage shoppingcart" )
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @ApiOperation(value = "Add a product in shopping cart", response = ResponseEntity.class)
    @PutMapping("/{barCode}/{email}/{id}")
    public ResponseEntity putProductInShoppingCart(
            @ApiParam(value = "Barcode of product", required = true) @PathVariable String barCode,
            @ApiParam(value = "Owner's email of shoppingcart", required = true) @PathVariable String email,
            @ApiParam(value = "Shoppingcart ID", required = true) @PathVariable int id )
    {
        shoppingCartService.addProduct(id,email,barCode);
        return ResponseEntity.ok("Element ajouté au panier");
    }

    @ApiOperation(value = "Delete a product in shopping cart", response = ResponseEntity.class)
    @DeleteMapping("/{barCode}/{email}/{id}")
    public ResponseEntity deleteProductInShoppingCart(
            @ApiParam(value = "Barcode of product", required = true) @PathVariable String barCode,
            @ApiParam(value = "Owner's email of shoppingcart", required = true) @PathVariable String email,
            @ApiParam(value = "Shoppingcart ID", required = true) @PathVariable int id )
    {
        shoppingCartService.deleteProduct(id,email,barCode);
        return ResponseEntity.ok("Element supprimé du panier");
    }

    @ApiOperation(value = "Return information about a shopping cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of shoppingcart quality", response = QualityResult.class),
            @ApiResponse(code = 404, message = "Not found shoppingcart"),
            @ApiResponse(code = 500, message = "Internal server error")}
    )
    @GetMapping("/{email}/{id}")
    public ResponseEntity getShoppingCartMark(
            @ApiParam(value = "Owner's email of shoppingcart", required = true) @PathVariable String email,
            @ApiParam(value = "Shoppingcart ID", required = true)@PathVariable int id )
    {
        ResponseEntity retour;
        QualityResult result = shoppingCartService.getQuality(id,email);
        if (result != null)
        {
            retour = ResponseEntity.ok(result);
        }
        else
        {
            retour = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return retour;
    }
}
