package org.isima.controllers;

import io.swagger.annotations.*;
import org.isima.models.QualityResult;
import org.isima.repositories.RuleRepository;
import org.isima.services.QualityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product")
@Api(value = "Product management", description = "Operations to have information about one product")
public class ProductController {

    @Autowired
    private QualityService qualityService;


    @ApiOperation(value = "Response is the quality of one product. The product is given thank a barcode")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of product quality", response = QualityResult.class)}
    )
    @GetMapping("/{barCode}")
    public ResponseEntity getMarkByBarCode( @ApiParam(value = "Barcode of product", required = true) @PathVariable String barCode) {
        QualityResult result = qualityService.getQuality(barCode);
        ResponseEntity response;
        if ( result == null)
        {
            response =  new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else
        {
            response = ResponseEntity.ok(result);
        }
        return response;
    }


}
