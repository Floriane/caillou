package org.isima.components;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.isima.models.Product;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class OpenFoodFactsComponent {

    public static JSONObject getProductApi(String barCode) {

        JSONObject product = null;
        String path = "https://fr.openfoodfacts.org/api/v0/produit/" + barCode + ".json";
        try {
            HttpResponse<JsonNode> jsonResponse
                    = Unirest.get(path)
                    .header("accept", "application/json")
                    .asJson();
            product = jsonResponse.getBody().getObject();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return product;
    }
}
