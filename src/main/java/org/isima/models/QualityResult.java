package org.isima.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import java.util.ArrayList;


@ApiModel(description = "All informations about quality of one product. ")
public class QualityResult {

    @ApiModelProperty(notes = "The mark of the product")
    public int mark;
    @ApiModelProperty(notes = "The list of advantage")
    public ArrayList<String> advantages;
    @ApiModelProperty(notes = "The list of default")
    public ArrayList<String> defaults;

    public QualityResult(int mark, ArrayList<String> advantages, ArrayList<String> defaults)
    {
        this.mark = mark;
        this.advantages = advantages;
        this.defaults = defaults;
    }

    public QualityResult()
    {
        this.mark = 0;
        this.advantages = new ArrayList<String>();
        this.defaults = new ArrayList<String>();
    }
}
