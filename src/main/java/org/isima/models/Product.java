package org.isima.models;

import java.util.ArrayList;

public class Product {

    public ArrayList<Nutriment> nutriments = new ArrayList<Nutriment>();

    public Product(double energy_100g, double saturated_fat_100g, double sugars_100g, double salt_100g, double fiber_100g, double proteins_100g)
    {
        nutriments.add(new Nutriment("energy_100g",energy_100g));
        nutriments.add(new Nutriment("saturated-fat_100g",saturated_fat_100g));
        nutriments.add(new Nutriment("sugars_100g",sugars_100g));
        nutriments.add(new Nutriment("salt_100g",salt_100g));
        nutriments.add(new Nutriment("fiber_100g",fiber_100g));
        nutriments.add(new Nutriment("proteins_100g",proteins_100g));
    }
}
