package org.isima.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "rule")
public class Rule {

    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Size(max = 2)
    @Column(name = "points", length = 2)
    private int points;

    @Size(max = 5)
    @Column(name = "min_bound", length = 5)
    private double min_bound;

    @Size(max = 2)
    @Column(name = "component", length = 2)
    private String component;

    public int getPoints() {
        return points;
    }

    public String getComponent() {
        return component;
    }

}
