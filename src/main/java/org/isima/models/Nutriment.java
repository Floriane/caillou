package org.isima.models;

public class Nutriment {

    public String name;
    public double value;
    public int mark;
    public boolean isPositif;

    public Nutriment(String name, double value)
    {
        this.name = name;
        this.value = value;
    }

    public Nutriment(String name, double value, int mark, boolean isPositif)
    {
        this.name = name;
        this.value = value;
        this.mark = mark;
        this.isPositif = isPositif;
    }

}
