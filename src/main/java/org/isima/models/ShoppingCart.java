package org.isima.models;


import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "shoppingcart")
public class ShoppingCart {

    @Id
    @Column(name ="key", nullable = false)
    private int key;

    @Column(name = "id", nullable = false)
    private int id;

    @Size(max = 100)
    @Column(name = "email", length = 100)
    private String email;

    @Size(max = 20)
    @Column(name = "barcode", length = 20)
    private String barCode;

    public String getBarCode() {
        return barCode;
    }
}
