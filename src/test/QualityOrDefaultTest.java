import org.isima.models.Nutriment;
import org.isima.services.QualityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class QualityOrDefaultTest {


    private QualityService qualityService;

    @BeforeEach
    private void init()
    {
        qualityService = new QualityService();
    }

    @Test
    @DisplayName("Test Negatif nutriment is Quality with low limit")
    void testNegatifNutrimentQualityLowLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 0, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }


    @Test
    @DisplayName("Test Negatif nutriment is Quality with medium")
    void testNegatifNutrimentQualityMedium() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 2, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }

    @Test
    @DisplayName("Test Negatif nutriment is Quality with hight limit")
    void testNegatifNutrimentQualityHightLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 3, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }

    @Test
    @DisplayName("Test Negatif nutriment is Nothing")
    void testNegatifNutrimentNothing() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 5, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(0, retour);
    }

    @Test
    @DisplayName("Test Negatif nutriment is Default with low limit")
    void testNegatifNutrimentDefaultLowLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 7, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(-1, retour);
    }

    @Test
    @DisplayName("Test Negatif nutriment is Default medium")
    void testNegatifNutrimentDefaultMedium() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 9, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(-1, retour);
    }

    @Test
    @DisplayName("Test Negatif nutriment is Default hight limit")
    void testNegatifNutrimentDefaultHightLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 10, false);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(-1, retour);
    }

    @Test
    @DisplayName("Test Positif nutriment is Default")
    void testNPositifNutrimentDefault() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 0, true);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(-1, retour);
    }

    @Test
    @DisplayName("Test Positif nutriment is Nothing")
    void testPositifNutrimentNothing() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 1, true);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(0, retour);
    }

    @Test
    @DisplayName("Test Positif nutriment is quality with low limit")
    void testPositifNutrimentQualityLowLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 2, true);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }

    @Test
    @DisplayName("Test Positif nutriment is quality medium")
    void testPositifNutrimentQualityMedium() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 3, true);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }

    @Test
    @DisplayName("Test Positif nutriment is quality with hight limit")
    void testPositifNutrimentQualityHightLimit() {
        Nutriment nutriment = new Nutriment("Test", 3.0, 5, true);
        int retour = qualityService.isQuality(nutriment);

        assertEquals(1, retour);
    }
}
