import org.isima.models.Product;
import org.isima.models.QualityResult;
import org.isima.services.QualityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class QualityProductTest {

    private QualityService qualityService;
    private Product product;

    @BeforeEach
    private void init()
    {
        qualityService = new QualityService();
        product = new Product(334, 0,4,80,5,8.1);
    }

    @Test
    @DisplayName("Test all advantages")
    void testAllAdvantges() {
        // intialisation du produit (note et nutriment positif ou non)
        // nutriment negatif
        for(int i = 0; i < 4 ; i ++)
        {
            product.nutriments.get(i).mark = 0;
            product.nutriments.get(i).isPositif = false;
        }

        // nutrinment positif
        for(int i = 0; i < 2; i ++)
        {
            product.nutriments.get(4 + i).mark = 5;
            product.nutriments.get(4 + i).isPositif = true;
        }
        QualityResult retour = qualityService.getQualityResultOfProduct(product);

        assertEquals(6, retour.advantages.size());
        assertEquals(0, retour.defaults.size());
        assertEquals(-10, retour.mark);
    }

    @Test
    @DisplayName("Test all default")
    void testAllDefault() {
        // intialisation du produit (note et nutriment positif ou non)
        // nutriment negatif
        for(int i = 0; i < 4 ; i ++)
        {
            product.nutriments.get(i).mark = 10;
            product.nutriments.get(i).isPositif = false;
        }

        // nutrinment positif
        for(int i = 0; i < 2; i ++)
        {
            product.nutriments.get(4 + i).mark = 0;
            product.nutriments.get(4 + i).isPositif = true;
        }
        QualityResult retour = qualityService.getQualityResultOfProduct(product);

        assertEquals(0, retour.advantages.size());
        assertEquals(6, retour.defaults.size());
        assertEquals(40, retour.mark);
    }

    @Test
    @DisplayName("Test all nothing")
    void testAllNothing() {
        // intialisation du produit (note et nutriment positif ou non)
        // nutriment negatif
        for(int i = 0; i < 4 ; i ++)
        {
            product.nutriments.get(i).mark = 5;
            product.nutriments.get(i).isPositif = false;
        }

        // nutrinment positif
        for(int i = 0; i < 2; i ++)
        {
            product.nutriments.get(4 + i).mark = 1;
            product.nutriments.get(4 + i).isPositif = true;
        }
        QualityResult retour = qualityService.getQualityResultOfProduct(product);

        assertEquals(0, retour.advantages.size());
        assertEquals(0, retour.defaults.size());
        assertEquals(18, retour.mark);
    }
}
